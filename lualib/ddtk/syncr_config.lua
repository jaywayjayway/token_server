-- Config of Sync Redis master
-- Written by Kevin.XU
-- 2016/8/1

local syncr = require "libsyncr" 

local _M = {
    _VERSION = '0.1'
}


local data = {
    ["SHOPPING"] = {
        ["queue_path"] = "/tmp/redis_sync_queue_shopping",
        ["msg_id"] = 0,
    },
    ["DEAL"] = {
        ["queue_path"] = "/tmp/redis_sync_queue_deal",
        ["msg_id"] = 0,
    }
}

-- sync_init
function _M.sync_init()
    for key,value in pairs(data) do  
        local queue_path = value.queue_path
        local msgid_tmp,err = syncr.sync_init(queue_path)
        if msgid_tmp < 0 then
            ngx.log(ngx.ERR, "sync_init failed : ", msgid_tmp, ", ", queue_path, ", ", err)
        else
            value.msg_id = msgid_tmp
        end
    end
end

-- get_msgid
function _M.get_msgid(service_type)
    return data[service_type].msg_id
end

return _M